import React from "react";

const ProductsStatistics = () => {
  return (
    <div className="col-xl-6 col-lg-12">
      <div className="card mb-4 shadow-sm">
        <article className="card-body">
          <h5 className="card-title">Thống kê món ăn</h5>
          <iframe 
            style={{ 
              background: "#FFFFFF",
              border: "none",
              borderRadius: "2px",
              boxShadow:"0 2px 10px 0 rgba(70, 76, 79, .2);",
              width:"100%",
              height:"350px",
              objectFit: "contain",
            }}
            src="https://charts.mongodb.com/charts-shop-mon-mon-iyaut/embed/charts?id=6298761c-840f-4a79-8715-4500f85b3904&maxDataAge=3600&theme=light&autoRefresh=true">
          </iframe>
        </article>
      </div>
    </div>
  );
};

export default ProductsStatistics;
