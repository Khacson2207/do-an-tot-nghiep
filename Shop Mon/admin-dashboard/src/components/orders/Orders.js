import React from "react";
import { Link } from "react-router-dom";
import moment from 'moment';

const Orders = (props) => {
  const { orders } = props;
  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Tên khách hàng</th>
          <th scope="col">Email</th>
          <th scope="col">Tổng tiền</th>
          <th scope="col">Trả</th>
          <th scope="col">Ngày tháng</th>
          <th>Trạng thái</th>
          <th scope="col" className="text-end">
            Xem
          </th>
        </tr>
      </thead>
      <tbody>
        {
          orders.map((order) => (
            <tr key={order._id}>
              <td>
                <b>{order.user.name}</b>
              </td>
              <td>{order.user.email}</td>
              <td>{order.totalPrice} </td>
              <td>
                {
                  order.isPaid ? (
                    <span className="badge rounded-pill alert-success">
                      Trả vào lúc {moment(order.paidAt).format("MM DD YY")}
                    </span>
                  ) : (
                    <span className="badge rounded-pill alert-danger">
                      Chưa trả
                    </span>
                  )
                }
              </td>
              <td>{moment(order.createdAt).format("MM DD YY")}</td>
              <td>
                {
                  order.isDelivered ? (
                    <span className="badge btn-success">Đã giao</span>
                  ) : (
                    <span className="badge btn-danger">Chưa giao</span>
                  )
                }
              </td>
              <td className="d-flex justify-content-end align-item-center">
                <Link to={`/order/${order._id}`} className="text-success">
                  <i className="fas fa-eye"></i>
                </Link>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );
};

export default Orders;
