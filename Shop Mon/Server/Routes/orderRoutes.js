import express from "express";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import Order from './../Models/OrderModel.js';
import { VNPay } from 'vn-payments';
import crypto from 'crypto';



const orderRouter = express.Router();


orderRouter.post(
    "/checkout",
    protect,
    asyncHandler(async (req, res) => {
        try {
            const instance = new VNPay({
                vnp_TmnCode: process.env.vnp_TmnCode,
                vnp_HashSecret: process.env.vnp_HashSecret,
            });
            const options = {
                amount: parseInt(params.amount.replace(/,/g, ''), 10),
                currency: "VND",
            };
            instance.orders.create(options, (error, order) => {
                if (error) {
                    console.log(error);
                    return res.status(500).json({ message: "Có gì đó không ổn" });
                }
                res.status(200).json({ data: order });
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Lỗi máy chủ nội bộ" });
        }
    })
);

orderRouter.post(
    "/verify",
    protect,
    asyncHandler(async (req, res) => {
        try {
            const { pay_order_id, pay_payment_id, pay_signature } =
                req.body;
            const sign = pay_order_id + "|" + pay_payment_id;
            const expectedSign = crypto
                .createHmac("sha256", process.env.vnp_HashSecret)
                .update(sign.toString())
                .digest("hex");
    
            if (razorpay_signature === expectedSign) {
                return res.status(200).json({ message: "Payment verified successfully" });
            } else {
                return res.status(400).json({ message: "Invalid signature sent!" });
            }
        } catch (error) {
            res.status(500).json({ message: "Internal Server Error!" });
            console.log(error);
        }
    })
);
//CREATE ORDER
orderRouter.post("/",
    protect,
    asyncHandler(async (req, res) => {
        const {
            orderItems,
            shippingAddress,
            paymentMethod,
            itemsPrice,
            taxPrice,
            shippingPrice,
            totalPrice
        } = req.body;
        if (orderItems && orderItems.length === 0) {
            res.status(400).json({ success: false, message: "No order items" });
            // throw new Error("No order items");
            return;
        }
        else {
            const order = new Order({
                orderItems,
                user: req.user._id,
                shippingAddress,
                paymentMethod,
                itemsPrice,
                taxPrice,
                shippingPrice,
                totalPrice
            });
            const createOrder = await order.save();
            res.status(201).json(createOrder);
        }
    })
);

//Admin get all orders
orderRouter.get(
    "/all",
    protect,
    admin,
    asyncHandler(async (req, res) => {
        const orders = await Order.find({})
            .sort({ _id: -1 })
            .populate("user", "id name email");
        res.json(orders);
    })
);

//GET ORDER BY ID
orderRouter.get(
    "/:id",
    protect,
    asyncHandler(async (req, res) => {
        const order = await Order.findById(req.params.id).populate(
            "user",
            "name email"
        )
        if (order) {
            res.json(order);
        }
        else {
            res.status(404).json({ success: false, message: "Order Not Found" });
        }
    })
);


//USER LOGIN ORDERS
orderRouter.get(
    "/",
    protect,
    asyncHandler(async (req, res) => {
        const order = await Order.find({ user: req.user._id }).sort({ _id: -1 });
        res.json(order);
    })
);

//ORDER IS PAID
orderRouter.put(
    "/:id/pay",
    protect,
    asyncHandler(async (req, res) => {
        const order = await Order.findById(req.params.id)
        if (order) {
            order.isPaid = true;
            order.paidAt = Date.now();
            order.paymentResult = {
                id: req.body.id,
                status: req.body.status,
                update_time: req.body.update_time,
                email_address: req.body.email_address,
            };
            const updatedOrder = await order.save();
            res.json(updatedOrder);
        }
        else {
            res.status(404).json({ success: false, message: "Order Not Found" });
        }
    })
);

//ORDER IS DELIVERE
orderRouter.put(
    "/:id/delivered",
    protect,
    asyncHandler(async (req, res) => {
        const order = await Order.findById(req.params.id)
        if (order) {
            order.isDelivered = true;
            order.deliveredAt = Date.now();

            const updatedOrder = await order.save();
            res.json(updatedOrder);
        }
        else {
            res.status(404).json({ success: false, message: "Order Not Found" });
        }
    })
);


// // Order with VNPay
// orderRouter.post(
//     '/create_payment_url',
//     protect,
//     asyncHandler(async(req,res,next)=>{
//         var ipAddr = req.headers['x-forwarded-for'] ||
//             req.connection.remoteAddress ||
//             req.socket.remoteAddress ||
//             req.connection.socket.remoteAddress;
//     })
// );

export default orderRouter;