import express from "express";
import dotenv from "dotenv";
import connectDatabase from "./config/MongoDb.js";
import ImportData from "./Dataimport.js";
import productRoute from "./Routes/ProductRoutes.js";
import { errorHandler, notFound } from "./Middleware/Errors.js";
import userRouter from "./Routes/UserRoutes.js";
import orderRouter from "./Routes/orderRoutes.js";


dotenv.config();
connectDatabase();
const app = express();
app.use(express.json());
//API
app.use("/api/import", ImportData);
app.use("/api/products", productRoute);
app.use("/api/users", userRouter);
app.use("/api/orders", orderRouter);
app.get("/api/config/paypal", (req, res) => {
    res.send(process.env.PAYPAL_CLIENT_ID);
});

// //config vnpay
// app.get("/api/config/tmnCode", (req, res) => {
//     res.send(process.env.vnp_TmnCode);
// });
// app.get("/api/config/secretKey", (req, res) => {
//     res.send(process.env.vnp_HashSecret);
// });
// app.get("/api/config/vnpUrl", (req, res) => {
//     res.send(process.env.vnp_Url);
// });
// app.get("/api/config/returnUrl", (req,res)=>{
//     res.send(process.env.vnp_ReturnUrl);
// });




//Error Handler
app.use(notFound);
app.use(errorHandler);


app.get("/", (req, res) => {
    res.send("Api dang chay...");
});


//app listening
const PORT = process.env.PORT || 1000;

app.listen(PORT, console.log(`server run in port ${PORT}`)); 