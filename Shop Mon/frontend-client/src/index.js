import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import store from "./Redux/store";
import { Provider } from "react-redux";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpApi from 'i18next-http-backend';
import '../node_modules/flag-icon-css/css/flag-icons.min.css';
i18n
    .use(HttpApi)
    .use(initReactI18next) // passes i18n down to react-i18next
    .use(LanguageDetector)
    .init({
        supportedLngs: ['vn', 'en'],
        fallbackLng: 'vn',
        debug: false,
        //option for language detector
        detection: {
            order: ['path', 'cookie', 'htmlTag'],
            caches: ['cookie'],
        },
        backend: {
            loadPath: '/assets/locales/{{lng}}/translation.json',
        },
    });
const loadingMarkup = (
    <div className="py-4 text-center">
        <h2>Loading...</h2>
    </div>   
)
ReactDOM.render(
    <Suspense fallback={loadingMarkup}>
        <Provider store={store}>
            <App />
        </Provider>,
    </Suspense>, 
    document.getElementById("root")
);
