import moment from "moment";
import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Message from "../LoadingError/Error";
import Loading from "../LoadingError/Loading";
import { useTranslation } from "react-i18next";
import cookie from 'js-cookie';
const languages = [
  {
    code: 'vn',
    name: 'Vietnamese',
    country_code: 'vn'
  },
  {
    code: 'en',
    name: 'English',
    country_code: 'gb'
  },
];
const Orders = (props) => {
  const currentLanguageCode = cookie.get('i18next') || 'vn';
  const { t } = useTranslation();
  const currentLanguage = languages.find(l => l.code === currentLanguageCode);
  useEffect(() => {
  }, [currentLanguage])
  const { loading, error, orders } = props;
  return (
    <div className=" d-flex justify-content-center align-items-center flex-column">
      {
        loading ? (
          <Loading />
        ) : error ? (
          <Message variant="alert-danger">{error}</Message>
        )
          : (
            <>
              {
                orders.length === 0 ? (
                  <div className="col-12 alert alert-info text-center mt-3">
                    {t('No_Orders')}
                    <Link
                      className="btn btn-success mx-2 px-3 py-2"
                      to="/"
                      style={{
                        fontSize: "12px",
                      }}
                    >
                      {t('Start_Order_Food')}
                    </Link>
                  </div>
                ) : (
                  <div className="table-responsive">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>{t('Status')}</th>
                          <th>DATE</th>
                          <th>{t('Total')}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          orders.map((order) => (
                            <tr className={`${order.isPaid ? "alert-success" : "alert-danger"}`} key={order._id}>
                              <td>
                                <a href={`/order/${order._id}`} className="link">
                                  {order._id}
                                </a>
                              </td>
                              <td>{order.isPaid ? <>Paid</> : <>Not Paid</>}</td>
                              <td>
                                {
                                  order.isPaid
                                    ? moment(order.paidAt).calendar()
                                    : moment(order.createdAt).calendar()
                                }
                              </td>
                              <td>{order.totalPrice} ₫</td>
                            </tr>
                          ))
                        }
                      </tbody>
                    </table>
                  </div>
                )
              }
            </>
          )
      }


    </div>
  );
};

export default Orders;
