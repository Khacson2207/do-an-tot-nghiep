import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import Toast from './../LoadingError/Toast';
import Message from './../LoadingError/Error';
import Loading from './../LoadingError/Loading';
import { toast } from "react-toastify";
import { updateUserProfile } from "../../Redux/Actions/userActions";
import { useTranslation } from "react-i18next";
import cookie from 'js-cookie';
const languages = [
  {
    code: 'vn',
    name: 'Vietnamese',
    country_code: 'vn'
  },
  {
    code: 'en',
    name: 'English',
    country_code: 'gb'
  },
];

const ProfileTabs = () => {
  const currentLanguageCode = cookie.get('i18next') || 'vn';
  const { t } = useTranslation();
  const currentLanguage = languages.find(l => l.code === currentLanguageCode);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const toastId = React.useRef(null);

  const Toastobjects = {
    pauseOnFocusLoss: false,
    draggable: false,
    pauseOnHover: false,
    autoClose: 2000,
  };

  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.userDetails);
  const { loading, error, user } = userDetails;

  const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
  const { loading: updateLoading } = userUpdateProfile;

  useEffect(() => {
    if (user) {
      setName(user.name);
      setEmail(user.email);
    }
  }, [dispatch, user, currentLanguage]);

  const submitHandler = (e) => {
    e.preventDefault();
    //Password match
    if (password !== confirmPassword) {
      if (!toast.isActive(toastId.current)) {
        toastId.current = toast.error("Password does not match", Toastobjects);
      }
    }
    else {
      // UPDATE PROFILE
      dispatch(updateUserProfile({ id: user._id, name, email, password }));
      if (!toast.isActive(toastId.current)) {
        toastId.current = toast.success("Profile Updated", Toastobjects);
      }
    }
  };
  return (
    <>
      <Toast />
      {error && <Message variant="alert-danger">{error}</Message>}
      {loading && <Loading />}
      {updateLoading && <Loading />}
      <form className="row  form-container" onSubmit={submitHandler}>
        <div className="col-md-6">
          <div className="form">
            <label for="account-fn">{t('User_name')}</label>
            <input className="form-control" type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>

        <div className="col-md-6">
          <div className="form">
            <label for="account-email">{t('Email_address')}</label>
            <input className="form-control" type="email"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className="col-md-6">
          <div className="form">
            <label for="account-pass">{t('New_password')}</label>
            <input className="form-control" type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <div className="col-md-6">
          <div className="form">
            <label for="account-confirm-pass">{t('Confirm_passowrd')}</label>
            <input className="form-control" type="password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </div>
        </div>
        <button type="submit">{t('Update_profile')}</button>
      </form>
    </>
  );
};

export default ProfileTabs;
