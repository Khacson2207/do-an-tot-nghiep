import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import cookie from 'js-cookie';
const languages = [
  {
    code: 'vn',
    name: 'Vietnamese',
    country_code: 'vn'
  },
  {
    code: 'en',
    name: 'English',
    country_code: 'gb'
  },
];
const ContactInfo = () => {
  const currentLanguageCode = cookie.get('i18next') || 'vn';
  const { t } = useTranslation();
  const currentLanguage = languages.find(l => l.code === currentLanguageCode);
  useEffect(()=>{
  },[currentLanguage])
  return (
    <div className="contactInfo container">
      <div className="row">
        <div className="col-12 col-md-4 contact-Box">
          <div className="box-info">
            <div className="info-image">
              <i className="fas fa-phone-alt"></i>
            </div>
            <h5>{t('call_us')}</h5>
            <p>0879936791</p>
          </div>
        </div>
        <div className="col-12 col-md-4 contact-Box">
          <div className="box-info">
            <div className="info-image">
              <i className="fas fa-map-marker-alt"></i>
            </div>
            <h5>{t('address_shop')}</h5>
            <p>51 Quan Nhân Thanh Xuân Hà Nội</p>
          </div>
        </div>
        <div className="col-12 col-md-4 contact-Box">
          <div className="box-info">
            <div className="info-image">
              <i className="fas fa-fax"></i>
            </div>
            <h5>{t('Desk_shop')}</h5>
            <p>0868889999</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactInfo;
