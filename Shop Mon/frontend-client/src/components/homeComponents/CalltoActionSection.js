import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import cookie from 'js-cookie';
const languages = [
  {
    code: 'vn',
    name: 'Vietnamese',
    country_code: 'vn'
  },
  {
    code: 'en',
    name: 'English',
    country_code: 'gb'
  },
];
const CalltoActionSection = () => {
  const currentLanguageCode = cookie.get('i18next') || 'vn';
  const { t } = useTranslation();
  const currentLanguage = languages.find(l => l.code === currentLanguageCode);
  useEffect(()=>{
  },[currentLanguage])
  return (
    <div className="subscribe-section bg-with-black">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className="subscribe-head">
              <h2>{t('title_banner')}</h2>
              <p>{t('content_banner')}</p>
              {/* <form className="form-section">
                <input placeholder="Your Email..." name="email" type="email" />
                <input value="Yes. I want!" name="subscribe" type="submit" />
              </form> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CalltoActionSection;
