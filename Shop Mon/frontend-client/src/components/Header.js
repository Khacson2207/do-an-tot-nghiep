import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { logout } from "../Redux/Actions/userActions";
import { useTranslation } from "react-i18next";
import cookie from 'js-cookie';
import i18next from 'i18next';
const languages = [
  {
    code: 'vn',
    name: 'Vietnamese',
    country_code: 'vn'
  },
  {
    code: 'en',
    name: 'English',
    country_code: 'gb'
  },
];

const Header = () => {
  const currentLanguageCode = cookie.get('i18next') || 'vn';
  const { t } = useTranslation();
  const currentLanguage = languages.find(l => l.code === currentLanguageCode);
  const [keyword, setKeyword] = useState();
  const dispatch = useDispatch();
  let history = useHistory();
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;
  // //Translate
  // const { t } = useTranslation();
  const logoutHandler = () => {
    dispatch(logout());
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (keyword.trim()) {
      history.push(`/search/${keyword}`);
    }
    else {
      history.push("/");
    }
  };
  useEffect(() => {
  }, [currentLanguage])
  return (
    <div>
      {/* Top Header */}
      <div className="Announcement ">
        <div className="container">
          <div className="row">
            <div className="col-md-6 d-flex align-items-center display-none">
              <p>+879936791</p>
              <p>nguyenkhacson2207@gmail.com</p>
            </div>
            <div className=" col-12 col-lg-6 justify-content-center justify-content-lg-end d-flex align-items-center">
              <Link to="">
                <i className="fab fa-facebook-f"></i>
              </Link>
              <Link to="">
                <i className="fab fa-instagram"></i>
              </Link>
              <Link to="">
                <i className="fab fa-linkedin-in"></i>
              </Link>
              <Link to="">
                <i className="fab fa-youtube"></i>
              </Link>
              <Link to="">
                <i className="fab fa-pinterest-p"></i>
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* Header */}
      <div className="header">
        <div className="container">
          {/* MOBILE HEADER */}
          <div className="mobile-header">
            <div className="container ">
              <div className="row ">
                <div className="col-6 d-flex align-items-center">
                  <Link className="navbar-brand" to="/">
                    <img alt="logo" src="/images/logo.png" />
                  </Link>
                </div>
                <div className="col-6 d-flex align-items-center justify-content-end Login-Register">
                  {
                    userInfo ? (
                      <div className="btn-group">
                        <button
                          type="button"
                          className="name-button dropdown-toggle"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i class="fas fa-user"></i>
                        </button>
                        <div className="dropdown-menu">
                          <Link className="dropdown-item" to="/profile">
                            {t('profile')}
                          </Link>

                          <Link className="dropdown-item" to="#"
                            onClick={logoutHandler}
                          >
                            {t('logout')}
                          </Link>
                        </div>
                      </div>
                    )
                      :
                      (
                        <div className="btn-group">
                          <button
                            type="button"
                            className="name-button dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            <i class="fas fa-user"></i>
                          </button>
                          <div className="dropdown-menu">
                            <Link className="dropdown-item" to="/login">
                              {t('Login')}
                            </Link>

                            <Link className="dropdown-item" to="register">
                              {t('Register')}
                            </Link>
                          </div>
                        </div>
                      )
                  }

                  <Link to="/cart" className="cart-mobile-icon">
                    <i className="fas fa-shopping-bag"></i>
                    <span className="badge">{cartItems.length}</span>
                  </Link>
                </div>
                <div className="col-12 d-flex align-items-center">
                  <form onSubmit={submitHandler} className="input-group">
                    <input
                      type="search"
                      className="form-control rounded search"
                      placeholder={t('search')}
                      onChange={(e) => setKeyword(e.target.value)}
                    />
                    <button type="submit" className="search-button">
                      {t('search')}
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>

          {/* PC HEADER */}
          <div className="pc-header">
            <div className="row">
              <div className="col-md-3 col-4 d-flex align-items-center">
                <Link className="navbar-brand" to="/">
                  <img alt="logo" src="/images/logo.png" />
                </Link>
              </div>
              <div className="col-md-6 col-8 d-flex align-items-center">
                <form onSubmit={submitHandler} className="input-group">
                  <input
                    type="search"
                    className="form-control rounded search"
                    placeholder={t('search')}
                    onChange={(e) => setKeyword(e.target.value)}
                  />
                  <button type="submit" className="search-button">
                    {t('search')}
                  </button>
                </form>
              </div>
              <div className="col-md-3 d-flex align-items-center justify-content-end Login-Register">
                {
                  userInfo ? (
                    <div className="btn-group">
                      <button
                        type="button"
                        className="name-button dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Hi, {userInfo.name}
                      </button>
                      <div className="dropdown-menu">
                        <Link className="dropdown-item" to="/profile">
                          {t('profile')}
                        </Link>

                        <Link className="dropdown-item" to="#"
                          onClick={logoutHandler}
                        >
                          {t('logout')}
                        </Link>
                      </div>
                    </div>
                  )
                    :
                    (
                      <>
                        <Link to="/register">
                          {t('Register')}

                        </Link>
                        <Link to="/login">
                          {t('Login')}
                        </Link>
                      </>
                    )
                }
                <Link to="/cart">
                  <i className="fas fa-shopping-bag"></i>
                  <span className="badge">{cartItems.length}</span>
                </Link>
                <div className="btn-group">
                  <button
                    type="button"
                    className="name-button dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fa fa-globe" aria-hidden="true"></i>
                  </button>
                  <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    {languages.map(({ code, name, country_code }) => (
                      <li key={country_code}>
                        <button className="dropdown-item"
                          onClick={() => i18next.changeLanguage(code)}
                          disabled={code === currentLanguageCode}
                        >
                          <span className={`flag-icon flag-icon-${country_code} mx-2`}></span>
                          {name}
                        </button>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
